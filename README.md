# SABRE INTRODUCTORY EXERCISE

## Overview

The main goal of this exercise is to familiarize with the technology stack in the
Interact Refresh project. To do so, you have to build a simple search form that will
fetch players using a few filters.

### Instructions

1. Create a React application using **[create-react-app](https://github.com/facebook/create-react-app)**

2. Build a Redux application that will fetch for football players given a number of field.

3. Accomplish at least 80% coverage in Istanbul (default reporter in Jest) in all files inside src folder.

## Graphic Mock

![Graphic Mock](./.github/graphic_mock.png)

## Fields

- Name: Input. Only letters
- Position: Select. Possible values are:
  - Attacking Midfield
  - Central Midfield
  - Centre-Back
  - Centre-Forward
  - Centre-Forward
  - Defensive Midfield
  - Keeper
  - Left Midfield
  - Left Wing
  - Left-Back
  - Right-Back

- Age: Input. Only Number: Goes from 18 to 40.

You can use any React UI framework, it is up to you.

## Minimum Requirements

This items are required in order to consider the exercise

- Fetch data from **[this url](https://football-players-b31f2.firebaseio.com/players.json?print=pretty)** to retrieve all the data (you must write a function that will filter all the players)

- Use **redux-thunk** as a middleware in Redux.

- Use **reselect** to implement selector for your Redux store.

- Write unit test for React Component, testing what is being rendering, lifecycle and inner methods using **jest** and **enzyme**. (Do not use Snapshot whentesting render methods).

- Write unit test for Redux Objects: action creators, actions, reducers, selectors, middlewares, etc. using **jest** and **enzyme**.

### Nice to Have

- Write integration test for the main flow.

- Responsiveness.

- Proper Error handling.

- Structure your application in a standard way.(<https://jaysoo.ca/2016/02/28/organizing-redux-application/>)