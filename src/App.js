import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as dispatchPlayers from './players/actions/index.js';

import Header from './header/header';
import Players from './players/players';

import {
  appDataSelector,
  filteredPlayersSelector,
} from './selectors';

export class App extends Component {

  componentDidMount() {
    this.props.loadPlayers();
  }

  render = () => {
    const { players } = this.props;

    return (
      <div>
        <Header />
        <Players players={players} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    data: appDataSelector(state),
    players: filteredPlayersSelector(state),
  };
}

export default connect(
  mapStateToProps,
  dispatchPlayers
)(App);
