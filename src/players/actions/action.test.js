import * as mainAction from './index';
import { UPDATE_PLAYERS, FILTER_PLAYERS } from './../../constants';

describe('Main Action', () => {

  it('should dispatch UPDATE_PLAYERS on call', async () => {
    spyOn(mainAction, 'updatePlayers').and.returnValue(Promise.resolve());
    const mockDispatch = jest.fn();

    await mainAction.loadPlayers()(mockDispatch);

    // expect(mockDispatch).toHaveBeenCalledWith({
    //   type: UPDATE_PLAYERS,
    //   players: []
    // });

    expect(mockDispatch).toHaveBeenCalled();
  });

});
