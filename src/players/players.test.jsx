import Players from './players';

const MOCK_PLAYERS = [
  {
    contractUntil: "2022-06-30",
    dateOfBirth: "1993-05-13",
    jerseyNumber: 9,
    name: "Romelu Lukaku",
    nationality: "Belgium",
    position: "Centre-Forward"
  },
  {
    contractUntil: "2019-06-30",
    dateOfBirth: "1990-11-07",
    jerseyNumber: 1,
    name: "David de Gea",
    nationality: "Spain",
    position: "Keeper"
  },
  {
    contractUntil: "2021-06-30",
    dateOfBirth: "1987-02-22",
    jerseyNumber: 20,
    name: "Sergio Romero",
    nationality: "Argentina",
    position: "Keeper"
  }
];

describe('<Players />', () => {

  it('should render correct amount of players', () => {
    const wrapper = mount(
      <Players players={MOCK_PLAYERS} />
    );

    const renderedPlayers = wrapper.find('Player');

    expect(renderedPlayers.length).toEqual(MOCK_PLAYERS.length);
  });

});
