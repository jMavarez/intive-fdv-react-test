import React from 'react';
import dayjs from 'dayjs';

const Player = ({ player }) => (
  <tr>
    <td>{player.name}</td>
    <td>{player.position}</td>
    <td>{player.nationality}</td>
    <td>{dayjs(player.dateOfBirth).format('DD/MM/YYYY')}</td>
    <td>{player.jerseyNumber}</td>
    <td>{dayjs(player.contractUntil).format('DD/MM/YYYY')}</td>
  </tr>
);

export default Player;
