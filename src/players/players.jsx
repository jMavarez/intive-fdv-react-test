import React from 'react';

import Player from './player';

import './players.css';

const Players = ({ players }) => (
  <div>
    <table id="players">
      <tbody>
        <tr>
          <th>Name</th>
          <th>Position</th>
          <th>Nationality</th>
          <th>Date of Birth</th>
          <th>Jersey Number</th>
          <th>Contract</th>
        </tr>
        {
          players.map((player, i) =>
            <Player key={i} player={player} />
          )
        }
      </tbody>
    </table>
  </div>
);

export default Players;
