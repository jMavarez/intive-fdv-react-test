import mainReducer from './index';
import { UPDATE_PLAYERS, FILTER_PLAYERS } from './../../constants';

const DEFAULT_STATE = {
  players: [],
  filters: {
    name: "",
    position: "none",
    age: 0
  }
};

const MANY_PLAYERS = [
  { name: 'player1' },
  { name: 'player2' },
  { name: 'player3' }
];

const FILTERS = {
  name: "",
  position: "none",
  age: 0
};

describe('Main Reducer', () => {

  it('has default state', () => {
    expect(mainReducer())
      .toEqual(DEFAULT_STATE);
  });

  it('can handle UPDATE_PLAYERS', () => {
    expect(mainReducer(undefined, {
      type: UPDATE_PLAYERS,
      players: MANY_PLAYERS
    })).toEqual({ ...DEFAULT_STATE, players: MANY_PLAYERS });
  });

  it('can handle FILTER_PLAYERS', () => {
    expect(mainReducer(undefined, {
      type: FILTER_PLAYERS,
      filters: FILTERS
    })).toEqual({ ...DEFAULT_STATE, filters: FILTERS });
  });

});
