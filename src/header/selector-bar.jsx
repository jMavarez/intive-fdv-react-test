import React from 'react';
import { connect } from 'react-redux';
import * as actionCreators from './../players/actions/index.js';

import PositionSelector from './position-selector.jsx';

export class SelectorBar extends React.Component {

  state = {
    nameFilter: "",
    positionFilter: "none",
    ageFilter: "",
  }

  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    if (name === "nameFilter") {
      if (/^[a-zA-Z\s]*$/.test(value)) {
        this.setState({ nameFilter: value });
      }
    } else if (name === "ageFilter") {
      this.setState({ ageFilter: value });
    }
  }

  handlePlayerPositionChange = (event) => {
    this.setState({ positionFilter: event.target.value });
  }

  getFilteredPlayers = () => {
    this.props.filterPlayers({
      name: this.state.nameFilter,
      position: this.state.positionFilter,
      age: parseInt(this.state.ageFilter, 10),
    });
  }

  render() {
    const { nameFilter, positionFilter, ageFilter } = this.state;

    return (
      <div id="filterBar">
        <input id="nameFilter" name="nameFilter" placeholder="Player Name" type="text" value={nameFilter} onChange={this.handleChange}></input>
        <PositionSelector positionFilter={positionFilter} handlePlayerPositionChange={this.handlePlayerPositionChange} />
        <input id="ageFilter" name="ageFilter" placeholder="Age" type="number" value={ageFilter} onChange={this.handleChange}></input>
        <button id="searchButton" onClick={this.getFilteredPlayers}>Search</button>
      </div>
    )
  }
}

const mapState = (state) => ({ filterPlayers: state.filterPlayers });

export default connect(mapState, actionCreators)(SelectorBar);
