import React from 'react';

import { PLAYER_POSITIONS } from '../constants';

const PositionSelector = ({ positionFilter, handlePlayerPositionChange }) => (
  <select value={positionFilter} onChange={handlePlayerPositionChange}>
    <option key={0} value="none">Position</option>
    {
      PLAYER_POSITIONS.map((position, i) =>
        <option key={i + 1} value={position}>{position}</option>
      )
    }
  </select>
)

export default PositionSelector;
