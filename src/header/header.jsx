import React from 'react';

import Title from './title';
import SelectorBar from './selector-bar';

import './header.css';

const Header = () => (
  <div id="headerWrapper">
    <Title>Football Player Finder</Title>
    <SelectorBar />
  </div>
);

export default Header;

