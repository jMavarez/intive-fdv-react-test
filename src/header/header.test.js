import Header from './header';

const initialState = {};
const mockStore = configureStore();

let store, wrapper;

describe('<Header />', () => {

  beforeEach(() => {
    store = mockStore(initialState);
    wrapper = shallow(<Header store={store} />);
  });

  it('should render title and selectorBar', () => {
    const title = wrapper.find("Title");
    const selectorBar = wrapper.find("SelectorBar");

    expect(title).not.toBeNull();
    expect(selectorBar).not.toBeNull();
  });

});
