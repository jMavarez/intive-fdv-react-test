import Title from './title';

const MOCK_TITLE = 'Football Player Finder';

it("should have correct title", () => {
  const wrapper = shallow(
    <Title>{MOCK_TITLE}</Title>
  );

  const title = wrapper.find("h2").text();

  expect(title).toEqual(MOCK_TITLE);
});
