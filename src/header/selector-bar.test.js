import { Provider } from 'react-redux';

import ConnectedSelectorBar, { SelectorBar } from './selector-bar';
import selectorBar from './selector-bar';

const initialState = {};
const mockStore = configureStore();

describe('<SelectorBar />', () => {

  let store, wrapper, simpleWrapper, props = {};

  beforeEach(() => {
    props = {
      filterPlayers: sinon.spy()
    };

    store = mockStore(initialState);
    wrapper = shallow(<ConnectedSelectorBar store={store} {...props} />);
    simpleWrapper = shallow(<SelectorBar {...props} />);
  });

  it("should call handleChange nameFilter", () => {
    const event = {
      target: { name: "nameFilter", value: "David" }
    };

    const container = mount(<SelectorBar {...props} />);
    const handleChange = jest.spyOn(container.instance(), 'handleChange');
    container['handleChange'] = handleChange

    container.update();
    container.find('#nameFilter').simulate('change', event);

    expect(handleChange).toBeCalled();
  });

  it("should call handleChange ageFilter", () => {
    const event = {
      target: { name: "ageFilter", value: "10" }
    };

    const container = mount(<SelectorBar {...props} />);
    const handleChange = jest.spyOn(container.instance(), 'handleChange');
    container['handleChange'] = handleChange

    container.update();
    container.find('#ageFilter').simulate('change', event);

    expect(handleChange).toBeCalled();
  });

  it("should call filterPlayers on click", () => {
    simpleWrapper.find('#searchButton').simulate('click');

    expect(props.filterPlayers.called).toEqual(true);
  });

});
