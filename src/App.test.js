import { Provider } from 'react-redux';

import ConnectedApp, { App } from './App';

const initialState = {
  players: [],
  filters: {
    name: "Sergio",
    position: "Keeper",
    age: 30
  }
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<App />', () => {

  let store, wrapper, props = {}, loadPlayers;

  beforeEach(() => {
    props = {
      players: [],
      loadPlayers: () => { },
    };

    loadPlayers = sinon.stub(props, 'loadPlayers');

    store = mockStore(initialState);
    wrapper = shallow(<ConnectedApp {...props} store={store} />);
  });

  it('render the connected component', () => {
    expect(wrapper.length).toEqual(1)
  });

  it('should call componentDidMount', () => {
    const componentDidMountSpy = jest.spyOn(ConnectedApp.prototype, 'componentDidMount');
    mount(<Provider store={store}><ConnectedApp {...props} /></Provider>);

    expect(componentDidMountSpy).toHaveBeenCalledTimes(1);

    componentDidMountSpy.mockRestore();
  });

  // it('should call loadPlayers on componentDidMount', async () => {
  //   const componentDidMountSpy = jest.spyOn(ConnectedApp.prototype, 'componentDidMount');
  //   const container = mount(<Provider store={store}><ConnectedApp {...props} /></Provider>);

  //   // expect(componentDidMountSpy).toHaveBeenCalledTimes(1);
  //   await container.instance().componentDidMount();
  //   expect(loadPlayers.calledOnce).toEqual(true);

  //   componentDidMountSpy.mockRestore();
  // });

  it('check props matches with initialState', () => {
    expect(wrapper.prop('players')).toEqual(initialState.players);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}><App {...props} /></Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  afterEach(() => {
    loadPlayers.reset();
  });

});
