import React from "react";
import ReactDOM from "react-dom";
import thunk from 'redux-thunk';
import Enzyme, { shallow, render, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import sinon from "sinon";
import configureStore from 'redux-mock-store';

Enzyme.configure({ adapter: new Adapter() });

global.React = React;
global.ReactDOM = ReactDOM;
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.sinon = sinon;
global.configureStore = configureStore;
global.thunk = thunk;
