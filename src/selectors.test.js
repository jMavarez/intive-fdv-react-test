import { type } from 'ramda';

import { appDataSelector, filtersSelector, playersSelector, filteredPlayersSelector } from './selectors';

let mockParameters;

const MOCK_PLAYERS = [
  {
    contractUntil: "2022-06-30",
    dateOfBirth: "1993-05-13",
    jerseyNumber: 9,
    name: "Romelu Lukaku",
    nationality: "Belgium",
    position: "Centre-Forward"
  },
  {
    contractUntil: "2019-06-30",
    dateOfBirth: "1990-11-07",
    jerseyNumber: 1,
    name: "David de Gea",
    nationality: "Spain",
    position: "Keeper"
  },
  {
    contractUntil: "2021-06-30",
    dateOfBirth: "1987-02-22",
    jerseyNumber: 20,
    name: "Sergio Romero",
    nationality: "Argentina",
    position: "Keeper"
  }
];

const MOCK_FILTERS = {
  name: "Sergio",
  position: "Keeper",
  age: 30
};

describe('Main Selectors', () => {

  it('should return appData as Object', () => {
    mockParameters = {
      players: MOCK_PLAYERS,
      filters: MOCK_FILTERS,
    };

    const func = appDataSelector(mockParameters);

    expect(func).not.toBeNull();
    expect(type(func)).toEqual('Object');
  });

  it('should return filters as Object', () => {
    mockParameters = {
      filters: MOCK_FILTERS,
    };

    const func = filtersSelector(mockParameters);

    expect(func).not.toBeNull();
    expect(type(func)).toEqual('Object');
  });

  it('should return filteredPlayers as Array', () => {
    mockParameters = {
      players: MOCK_PLAYERS,
      filters: MOCK_FILTERS,
    };

    const func = filteredPlayersSelector.resultFunc(mockParameters.players, mockParameters.filters);

    expect(func).not.toBeNull();
    expect(type(func)).toEqual('Array');
  });

  it('should return players as Array', () => {
    mockParameters = {
      players: MOCK_PLAYERS,
    };

    const func = playersSelector(mockParameters);

    expect(func).not.toBeNull();
    expect(type(func)).toEqual('Array');
  });

});
